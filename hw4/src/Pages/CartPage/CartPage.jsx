import React from "react";
import s from './CartsPage.module.scss';
import CartItems from "../../components/CartItems/CartItems";

function CartPage(props) {
  const {name, count, removeItem, openModal} = props
  return (
   <>
   <CartItems name={name} count={count} removeItem={removeItem} openModal={openModal} />
   </>
  );
}

export default CartPage