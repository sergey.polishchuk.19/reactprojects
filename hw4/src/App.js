import React, {useEffect, useState} from "react";
import {useSelector, useDispatch} from "react-redux";
import axios from "axios";
import './App.css';
import {Route, Switch} from "react-router-dom";
import Cart from "./components/Cart/Carts";
import HeaderTop from "./components/Header/HeaderTop/HeaderTop";
import HeaderMiddle from "./components/Header/HeaderMiddle/HeaderMiddle";
import Modal from "./components/Modal/Modal";
import Footer from "./components/Footer/Footer";
import CartPage from "./Pages/CartPage/CartPage";
import MainPage from "./Pages/MainPage/MainPage";
import FavPage from "./Pages/FavPage/FavPage";
import {getData, setCards, setOpen, getDataToCart, getDataToFav} from "./AppStore/actionsCreator/actionsCreator";


function App() {
  const dispatch = useDispatch()
  const cards = useSelector((state) => {
    return state.items.cards
  })
  const favorite = useSelector(state =>{
    return state.items.favorite
  })
  const carts = useSelector((state) => {
    return state.cart.cartItems
  })
  const modal = useSelector((state) => {
    return state.modal.isOpen
  })
  const [current, setCurrent] = useState('')
  const [modalType, setModalType] = useState('')

  useEffect(() => {
    const arrayCards = JSON.parse(localStorage.getItem('cards'))
    const arrayCart = JSON.parse(localStorage.getItem('carts'))
    const arrayFav = JSON.parse(localStorage.getItem('favorite'))
    if(arrayCards || arrayCart.length < 1){
      dispatch(getData())
    } else {
      dispatch(setCards())
    } if (arrayCart){
      dispatch(getDataToCart(arrayCart))
      dispatch(getDataToFav(arrayFav))
    }
  }, [])
  useEffect(() => {
    localStorage.setItem('cards', JSON.stringify(cards))
    localStorage.setItem('carts', JSON.stringify(carts))
    localStorage.setItem('favorite', JSON.stringify(favorite))
  }, [cards, carts, favorite])

  const openModal = (name, type = 'addToCart') => {
    if (type === 'addToCart') {
      setModalType('addItem')
      dispatch(setOpen(true))
      setCurrent(name)
    } else {
      setModalType('deleteItem')
      dispatch(setOpen(true))
      setCurrent(name)
    }
  }
  const closeModal = () => {
    dispatch(setOpen(false))
  }

  return (
   <div className="App">
     <Cart carts={carts} cards={favorite}/>
     <header>
       <HeaderTop/>
       <HeaderMiddle/>
     </header>
     <main>
       <Modal current={current}
              modalType={modalType}
              modal={modal}
              closeModal={closeModal}/>
       <Switch>
         <Route exact path="/home">
           <MainPage cards={cards}
                     openModal={openModal}
                     modalType={modalType}/>
         </Route>
         <Route exact path="/cart">
           {carts && carts.map((element) => <CartPage openModal={openModal} {...element}/>)}
         </Route>
         <Route>
           <FavPage favorite={favorite}/>
         </Route>
       </Switch>
     </main>
     <Footer/>
   </div>
  );
}

export default App;
