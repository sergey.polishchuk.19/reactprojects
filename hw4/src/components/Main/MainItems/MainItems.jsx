import React from "react";
import s from './MainItems.module.scss'
import Item from "./Item";

function MainItems (props) {
  const {cards, addCarts, openModal, removeItemFav, addFavorites} = props

    return (
     <>
       <h2 className={s.playlistLogo}>Latest Arrivals in musica</h2>
       <section className={s.playlist}>
         {cards && cards.map((elements) => <Item openModal={openModal} {...elements} addCarts={addCarts} removeItemFav={removeItemFav} addFavorites={addFavorites}/>)}
       </section>
     </>
    );
  }

export default MainItems