import React from "react";
import s from "./MainItems.module.scss";
import {ReactComponent as StarSolid} from "../../Favorites/icon/star-solid.svg";
import {ReactComponent as StarAdd} from "../../Favorites/icon/star-solid-add.svg";
import {addToFavorites} from "../../../AppStore/actionsCreator/actionsCreator";
import {useDispatch} from "react-redux";

function Item (props) {
  const {name, url, value, id, isFavorite, openModal, removeItemFav, addFavorites} = props
  const dispatch = useDispatch()
    return(
     <>
        <div>
          <div className={s.wrapper}>
            <img className={s.listImg} src={url} alt="icon album"/>
            <div className={s.listItem}>
              <div className={s.listHead}>
                <h3>{name}</h3>
                <span>by Artist</span>
              </div>
              <p className={s.listDesc}><span>Lorem ipsum</span> dolor sit amet, con
                adipiscing elit, sed diam nonu.</p>
              <div className={s.listInfo}>
                <p>${value}</p>
                <div className={s.wrapperAction}>
                  {isFavorite ? <StarAdd style={{ width: '25px', cursor: 'pointer' }} onClick={()=> dispatch(addToFavorites(name, url, value, id)) } /> : <StarSolid style={{ width: '25px', cursor: 'pointer' }} onClick={()=> dispatch(addToFavorites(name, url, value, id))} />}
                <button className={s.btnAdd} onClick={()=>{
                  openModal(name)
                }}>Add to cart</button>
                </div>
              </div>
            </div>

          </div>
        </div>
     </>
    )
  }

export default Item