import './App.scss';
import React from "react";
import Modal from "./components/Modal/Modal";


class App extends React.Component {
  state = {
    firstModal: false,
    secondModal: false,
  }

  closeFirstModal = () => {
    this.setState({
      firstModal: false,
    })
  }
  openFirstModal = () => {
    this.setState({
      firstModal: true,
    })

  }
  closeSecondModal = () => {
    this.setState({
      secondModal: false,
    })
  }
  openSecondModal = () => {
    this.setState({
      secondModal: true,
    })
  }

  render() {
    return (

     <div className="App" >

       {window.addEventListener('click', ()=>{
         this.closeFirstModal()
         this.closeSecondModal()
       })}

       <section onClick={event => event.stopPropagation()}>
         <button className='btnOpenModal' onClick={this.openFirstModal}>Open first modal</button>
         {this.state.firstModal && <Modal
          header='Do you want to delete this file?'
          text='Once you delete this file, it won’t be possible to undo this action.
             Are you sure you want to delete it?'
          textColor={{color: '#fff'}}
          closeModal={this.closeFirstModal}
          bg={{backgroundImage: 'linear-gradient(147deg, #FFE53B 0%, #FF2525 74%)'}}
          headerBg={{backgroundColor: '#d44637'}}
          colorBtn={{backgroundColor: '#b3382c'}}
          firstBtn='Ok'
          secondBtn='Cancel'/>
         }

         <button className='btnOpenModal' onClick={this.openSecondModal}>Open second modal</button>
         {this.state.secondModal && <Modal
          header='The Matrix'
          text='You take the blue pill - the story ends, you wake up in your bed and believe in what you want to believe. You take a red pill - you find yourself in a wonderland and I will show you how deep it is '
          textColor={{color: '#000'}}
          closeModal={this.closeSecondModal}
          bg={{backgroundColor: '#fff'}}
          headerBg={{backgroundColor: '#000'}}
          colorBtn={{backgroundColor: '#000'}}
          firstBtn='Red Pill'
          secondBtn='Blue pill'/>
         }
       </section>
     </div>
    );
  }
}

export default App;

