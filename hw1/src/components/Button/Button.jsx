import React from "react";
import s from './Button.module.scss'

class Button extends React.Component{
  render() {
    const {text, backgroundColor} = this.props
    return(

     <>
       <button className={s.btn} style={backgroundColor}>{text}</button>
     </>
    )
  }
}

export default Button