import React from "react";
import s from './Modal.module.scss'
import closePng from './imgs/cross.png'
import Button from "../Button/Button";

class Modal extends React.Component {

  render() {

    const {header, text, closeModal, textColor, bg, headerBg, colorBtn, firstBtn, secondBtn} = this.props;

    return (
     <form style={bg}>
       <header style={headerBg}>
         <span>{header}</span>
         <img className={s.close} src={closePng} alt="close button" onClick={closeModal}/>
       </header>
       <main>
         <span style={textColor}>{text}</span>
       </main>
       <footer>
         <Button backgroundColor={colorBtn} text={firstBtn}  />
         <Button backgroundColor={colorBtn} text={secondBtn} />
       </footer>
     </form>

    )
  }
}

export default Modal