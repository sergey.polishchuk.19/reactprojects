import React, {lazy, useEffect, useState} from "react";
import axios from "axios";
import './App.css';
import {Route, Switch} from "react-router-dom";
import Cart from "./components/Cart/Carts";
import HeaderTop from "./components/Header/HeaderTop/HeaderTop";
import HeaderMiddle from "./components/Header/HeaderMiddle/HeaderMiddle";
import Modal from "./components/Modal/Modal";
import Footer from "./components/Footer/Footer";
import CartPage from "./Pages/CartPage/CartPage";
import MainPage from "./Pages/MainPage/MainPage";
import FavPage from "./Pages/FavPage/FavPage";
import Routes from "./Routes/Routes";


function App() {
  const [cards, setCards] = useState(() => {
    const saved = localStorage.getItem('favorite');
    const initialState = JSON.parse(saved);
    return initialState || [];
  });
  const [carts, setCarts] = useState(() => {
    const saved = localStorage.getItem('carts');
    const initialState = JSON.parse(saved);
    return initialState || [];
  });
  const [modal, setModal] = useState(false);
  const [current, setCurrent] = useState('')
  const [favorite, setFavorite] = useState(() => {
    const saved = localStorage.getItem('favorite');
    const initialState = JSON.parse(saved);
    return initialState || [];
  });
  const [modalType, setModalType] = useState('')

  useEffect(() => {
    try {
      (async () => {
        const {data} = await axios('./library.json')
        setCards(data)
      })()
    } catch (e) {
      console.log(e)
    }
  }, [])
  useEffect(() => {
    localStorage.setItem('cards', JSON.stringify(cards))
    localStorage.setItem('carts', JSON.stringify(carts))
    localStorage.setItem('favorite', JSON.stringify(favorite))
  }, [carts, favorite])

  const openModal = (name, type = 'addToCart') => {
    if (type === 'addToCart') {
      setModalType('addItem')
      setModal(true)
      setCurrent(name)
      console.log(current)
    } else {
      setModalType('deleteItem')
      setModal(true)
      setCurrent(name)
    }
  }
  const closeModal = () => {
    setModal(false)
  }
  const addCarts = (name) => {
    const index = carts.findIndex(({name: arrName}) => {
      return name === arrName
    })
    if (index === -1) {
      setCarts((current) => [...current, {name, count: 1}])
    } else
      setCarts((current) => {
        const newState = [...current]
        newState[index].count = newState[index].count + 1
        return newState
      })
  }
  const addFavorites = (name, url, value, id) => {
    const index = cards.findIndex(({name: arrName}) => name === arrName)
    setCards((current) => {
      const newCards = [...current]
      if (newCards[index].isFavorite === false) {
        newCards[index].isFavorite = true
        setFavorite((current) => [...current, {name, url, value, id}])
        setCards(() => {
         return  newCards
        })
      } else {
        if(newCards[index] === newCards[index]){
        newCards[index].isFavorite = false
         localStorage.removeItem('favorite')
          favorite.splice(index, id)
        }
        setCards(() => newCards)
      }
    })

  }
  const removeItem = (name) => {
    const index = carts.findIndex(({name: arrName}) => {
      return name === arrName
    })
    const newState = [...carts]
    if (newState[index].count === 1) {
      newState.splice(index, 1)
    } else {
      newState[index].count = newState[index].count - 1
    }
    setCarts((current) => newState)
  }

  return (
   <div className="App">
     <Cart carts={carts} cards={favorite}/>
     <header>
       <HeaderTop/>
       <HeaderMiddle/>
     </header>
     <main>
       <Modal addCarts={addCarts}
              current={current}
              modalType={modalType}
              removeItem={removeItem}
              modal={modal}
              closeModal={closeModal}/>
       <Switch>
         <Route exact path="/home">
           <MainPage cards={cards}
                     addCarts={addCarts}
                     openModal={openModal}
                     addFavorites={addFavorites}
                     modalType={modalType}/>
         </Route>
         <Route exact path="/cart">
           {carts && carts.map((element) => <CartPage openModal={openModal} {...element} removeItem={removeItem}/>)}
         </Route>
         <Route>
           <FavPage favorite={favorite}/>
         </Route>
       </Switch>
     </main>
     <Footer/>
   </div>
  );
}

export default App;
