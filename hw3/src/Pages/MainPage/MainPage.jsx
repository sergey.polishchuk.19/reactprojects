import React from "react";
import ItemsContainer from "../../components/ItemsContainer/ItemsContainer";

function MainPage (props){
  const {cards,
    addCarts,
    openModal,
    removeItemFav,
    addFavorites} = props
  return(
   <div>
   <ItemsContainer  cards={cards} addCarts={addCarts} openModal={openModal} removeItemFav={removeItemFav} addFavorites={addFavorites}/>
   </div>
  )
}

export default MainPage