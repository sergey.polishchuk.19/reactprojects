import React from "react";
import Main from "../Main/Main";

function ItemsContainer (props) {
  const {cards, addCarts, openModal, removeItemFav, addFavorites} = props
  return(
   <>
   <Main cards={cards} addCarts={addCarts} openModal={openModal} removeItemFav={removeItemFav} addFavorites={addFavorites} />
   </>
  )
}

export default ItemsContainer