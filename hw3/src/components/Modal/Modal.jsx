import React from "react";
import s from "./Modal.module.scss"
import closeIcon from './icon/close.png'

function Modal(props) {
  const { addCarts, current, modalType, removeItem, modal, closeModal } = props

  if (!modal) return null
  let method
  let textModal
  if(modalType === 'addItem'){
    method = addCarts
    textModal = 'Add'
    console.log('add')
  } else if(modalType === 'deleteItem') {
    method = removeItem
    textModal = 'Remove'
    console.log('close')
  }
  return (
   <section>
     <div className={s.root}>
       <div className={s.background}/>
       <div className={s.content}>
         <div className={s.closeWrapper}>
           <button onClick={closeModal}>
             <img width='18' src={closeIcon} alt="icon close"/>
           </button>
         </div>
         <h2>{textModal}<span className={s.nameAlbum}>{current}</span>?</h2>
         <div className={s.buttonContainer}>
           <button onClick={() => {
             method(current)
             closeModal()
           }}>Yes
           </button>
           <button>No</button>
         </div>
       </div>
     </div>

   </section>
  )
}

export default Modal