import React from "react";
import s from './FavoriteItems.module.scss';
import iconClose from "../CartItems/icon/close.png";

function FavoriteItems(props) {
  const {favorite} = props

  return (
   <section>
     <div className={s.container}>
       <div className={s.wrapper}>
         {favorite.map(element => (
          <div key={element.id} className={s.wrapperItems}>
            <div className={s.item}>
              <img style={{width: '150px'}} src={element.url} alt="icon"/>
              <div className={s.infoItem}>
                <h3>{element.name}</h3>
                <span>by Artist</span>
              </div>
            </div>
            <span className={s.priceItem}>{element.value}$</span>
          </div>
         ))}
       </div>
     </div>
   </section>
  );
}

export default FavoriteItems