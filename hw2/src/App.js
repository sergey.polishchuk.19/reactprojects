import React from "react";
import './App.scss';
import HeaderTop from "./componets/Header/HeaderTop/HeaderTop";
import HeaderMiddle from "./componets/Header/HeaderMiddle/HeaderMiddle";
import Main from "./componets/Main/Main";
import Footer from "./componets/Footer/Footer";
import Cart from "./componets/Cart/Cart";
import Modal from "./componets/Modal/Modal";

class App extends React.Component {

  state = {
    cards: [],
    carts: [],
    currentCards: {},
    modal: false,
  }

  componentDidUpdate() {
    window.localStorage.setItem('carts', JSON.stringify(this.state))
  }

  async componentDidMount() {

    try {
      const state = window.localStorage.getItem('carts');
      this.setState({...JSON.parse(state)})
    } catch (err) {
      console.log(err)
    }

    try {
      const response = await fetch('./library.json')
       .then(e => e.json())
      console.log(response)

      this.setState({
        cards: response,
      })
    } catch (e) {
      console.log(e)
    }

  }

  closeModal = () => {
    this.setState({modal: false})
  }

  openModal = (element) => {
    this.setState((current) => {
      return {
        ...current,
        currentCards: element
      }
    })
    this.setState({
      modal: true
    })
  }

  addFavorites = (name) => {
    const index = this.state.cards.findIndex(({name: arrayName}) => {
      return name === arrayName;
    })
    const cards = [...this.state.cards];
    if (cards[index].isFavorite === false) {
      cards[index].isFavorite = true
      this.setState((current) => {
        return {
          ...current,
          cards: cards
        }
      })
    } else {
      cards[index].isFavorite = false
      this.setState((state) => {
        return {
          ...state,
          cards: cards
        }
      })

    }

  }

  addCarts = (element) => {
    const carts = this.state.carts
    const index = carts.findIndex(({element: name}) => {
      return element === name
    })
    if (index === -1) {
      this.setState((current) => ({
        ...current,
        carts: [
          ...current.carts,
          {
            element,
            count: 1
          }
        ]
      }))
    } else {
      carts[index].count++
      this.setState((state) => {
        return {
          ...state,
          carts: carts
        }
      })
    }
  }

  render() {
    const {cards, carts, modal, currentCards} = this.state

    return (
     <div className="App">
       <Cart carts={carts}/>
       <header>
         <HeaderTop/>
         <HeaderMiddle/>
       </header>
       <main><Modal name={currentCards} modal={modal} openModal={this.openModal} closeModal={this.closeModal} actions={
            <><button onClick={() => {
                this.addCarts(currentCards)
                this.closeModal()
              }}>Ok</button>
              <button onClick={() => this.setState({modal: false})}>Cancel</button>
            </>
          }
         />
         <Main cards={cards} openModal={this.openModal} addFavorites={this.addFavorites}/>
       </main>
       <Footer/>
     </div>
    );
  }
}

export default App;
