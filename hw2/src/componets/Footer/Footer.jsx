import React from "react";
import s from './Footer.module.scss'
import bg from './imgs/background.jpg'
import urlFacebook from './imgs/facebook.png'
import urlTwitter from './imgs/twitter.png'
import urlDribble from './imgs/icon-dribbble.png'
import urlDig from './imgs/099291-glossy-black-icon-social-media-logos-digg-logo.png'
import urlEmail from './imgs/078519-blue-jelly-icon-business-envelope5.png'
import topAlbumOne from './imgs/image-2.png'
import topAlbumTwo from './imgs/image.png'
import iconComment from './imgs/comments.png'

const aboutUs = [urlFacebook, urlTwitter, urlDribble, urlDig, urlEmail,]


class Footer extends React.Component {
  render() {
    return (
     <>
       <section className={s.footerWrapper} style={{backgroundImage: `url(${bg})`}}>
         <div className={s.container}>
           <div className={s.footerFlex}>
             <section className={s.aboutUs}>
               <h3>Little about us</h3>
               <p className={s.aboutDesc}>Sed posuere consectetur est at.
                 Nulla vitae elit libero, a pharetra.
                 Lorem ipsum dolor sit amet, conse
                 ctetuer adipiscing elit.</p>
               <p className={s.linksLogo}>Socialize with us</p>
               <div className={s.linksCompany}>
                 {aboutUs.map(elem => (
                  <div>
                    <img src={elem} alt={elem}/>
                  </div>
                 ))}
               </div>
             </section>
             <section className={s.archives}>
               <h2>Our Archives</h2>
               <p>March 2012</p>
               <p>February 2012</p>
               <p>January 2012</p>
               <p>December 2011</p>
             </section>
             <section className={s.popularPosts}>
               <h2>Popular Posts</h2>
               <div className={s.postsItems}>
                 <div className={s.itemsImg}>
                   <img src={topAlbumOne} alt="Image album"/>
                 </div>
                 <div>
                   <h3>Great Album</h3>
                   <div className={s.comments}>
                     <img src={iconComment} alt="icon"/>
                     <p>12 comments</p>
                   </div>
                 </div>
               </div>
               <div className={s.postsItems}>
                 <div className={s.itemsImg}>
                   <img src={topAlbumTwo} alt="Image album"/>
                 </div>
                 <div>
                   <h3>Great Album</h3>
                   <div className={s.comments}>
                     <img src={iconComment} alt="icon"/>
                     <p>12 comments</p>
                   </div>
                 </div>
               </div>
             </section>
             <section className={s.search}>
               <h2>Search our Site</h2>
               <form className={s.searchFrom}>
                 <input type="text" placeholder="Enter Search ..."/>
               </form>
               <h2>Tag Cloud</h2>
               <div className={s.tags}>
                 <span>Audio CD</span>
                 <span>Video</span>
                 <span>Playlist</span>
                 <span>Avantgarde</span>
                 <span>Malancholic</span>
               </div>
             </section>

           </div>
         </div>
       </section>
         <section className={s.subFooter}>
           <div className={s.container}>
             <ul>
               <li><a href="#">Home</a></li>
               <li><a href="#">Portfolio</a></li>
               <li><a href="#">Sitemap</a></li>
               <li><a href="#">Contact</a></li>
             </ul>
             <p>Musica @2013 by PremiumCoding | All Rights Reserved</p>
           </div>
         </section>
     </>
    );
  }
}

export default Footer