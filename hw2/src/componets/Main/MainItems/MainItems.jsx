import React from "react";
import PropTypes from "prop-types";
import s from './MainItems.module.scss'
import Item from "./Item";

class MainItems extends React.Component {

  render() {
    const {addCarts, cards, addCart, openModal, addFavorites} = this.props
    return (
     <>
       <h2 className={s.playlistLogo}>Latest Arrivals in musica</h2>
       <section className={s.playlist}>
          <Item openModal={openModal} addCarts={addCarts} cards={cards} addCart={addCart} addFavorites={addFavorites}/>
       </section>
     </>
    );
  }
}

MainItems.proptype = {
  addCarts: PropTypes.func.isRequired,
  cards: PropTypes.array.isRequired,
  addCart: PropTypes.func.isRequired,
  openModal: PropTypes.func.isRequired,
  addFavorites: PropTypes.func.isRequired,
}

export default MainItems