import React from "react";
import PropTypes from "prop-types";
import s from "./MainItems.module.scss";
import Favorites from "../../Favorites/Favorites";

class Item extends React.Component {


  render() {
    const {cards, openModal, addFavorites} = this.props
    return(
     <>
     {cards.map(element => (
      <div key={element.name}>
        <div className={s.listWrapper}>
          <div className={s.wrapper}>
            <img className={s.listImg} src={element.url} alt="Image album"/>
            <div className={s.listItem}>
              <div className={s.listHead}>
                <h3>{element.name}</h3>
                <span>by Artist</span>
              </div>
              <p className={s.listDesc}><span>Lorem ipsum</span> dolor sit amet, con
                adipiscing elit, sed diam nonu.</p>
              <div className={s.listInfo}>
                <p>${element.price}</p>
                <div className={s.wrapperAction}>
                <Favorites {...element} id={element.id} addFavorites={addFavorites}/>
                <button className={s.btnAdd} onClick={()=>{
                 openModal(element.name)
                }}>Add to cart</button>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
       ))}
     </>
    )
  }
}

Item.proptype = {
  cards: PropTypes.array.isRequired,
  openModal: PropTypes.func.isRequired,
  addFavorites: PropTypes.func.isRequired,
}

export default Item