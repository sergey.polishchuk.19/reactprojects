import React from "react";
import PropTypes from "prop-types";
import s from './Main.module.scss'
import cd from './imgs/cd.png'
import phones from './imgs/icon.png'
import calendar from './imgs/calendar.png'
import bg from '../imgsBase/bg-slider-second.jpg'
import companyOne from './imgs/1.png'
import companyTwo from './imgs/2.png'
import companyThree from './imgs/3.png'
import companyFour from './imgs/4.png'
import companyFive from './imgs/5.png'
import MainItems from "./MainItems/MainItems";

const companys = [
 {img: companyOne,
 id: 1
 },
  {img: companyTwo,
  id: 2
  },
  {img: companyThree,
  id: 3
  },
  {img: companyFour,
  id: 4
  },
  {img: companyFive,
  id: 5
  },
]
class Main extends React.Component {

  render() {
    const {addCarts, cards, addCart, openModal, addFavorites} = this.props
    return (
     <>
       <section className={s.mainWrapper} style={{backgroundImage: `url(${bg})`}}>
         <div className={s.container}>
           <header className={s.mainHeader}>
             <h2 className={s.mainLogo}>Welcome to <span>Musica,</span> check our lates albums</h2>
             <div className={s.mainLinks}>
               <a href="#" className={s.linksItem}>
                 <div >
                   <div className={s.linksHead}>
                     <img className={s.linksIcon} src={cd} alt="icon"/>
                     <h2 className={s.linksName}>Check our collection</h2>
                   </div>
                   <p className={s.linksDesc}>Donec pede justo, fringilla vel, al, vulputate
                     eget, arcu. In enim justo, lorem ipsum.</p>
                 </div>
               </a>
               <a href="#" className={s.linksItem}>
                 <div>
                   <div className={s.linksHead}>
                     <img className={s.linksIcon} src={phones} alt="icon"/>
                     <h2 className={s.linksName}>Listen before purchase</h2>
                   </div>
                   <p className={s.linksDesc}>Donec pede justo, fringilla vel, al, vulputate
                     eget, arcu. In enim justo, lorem ipsum.</p>
                 </div>
               </a>
               <a href="#" className={s.linksItem}>
                 <div>
                   <div className={s.linksHead}>
                   <img className={s.linksIcon} src={calendar} alt="icon"/>
                   <h2 className={s.linksName}>Upcoming events</h2>
                   </div>
                   <p className={s.linksDesc}>Donec pede justo, fringilla vel, al, vulputate
                     eget, arcu. In enim justo, lorem ipsum.</p>
                 </div>
               </a>

             </div>
           </header>
           <main className={s.mainBody}>
            <MainItems openModal={openModal} addCarts={addCarts} cards={cards} addCart={addCart} addFavorites={addFavorites}/>
           </main>
           <footer className={s.mainFooter}>
              <h2>Our most important publishers</h2>
             <div className={s.footerPublish}>
               {companys.map( elem => (
                <div>
                  <img key={elem.id} src={elem.img} alt="Icon company"/>
                </div>
               ))}
             </div>
           </footer>
         </div>

       </section>

     </>
    );
  }
}

Main.proptype = {
  addCarts: PropTypes.func.isRequired,
  cards: PropTypes.array.isRequired,
  addCart: PropTypes.func.isRequired,
  openModal: PropTypes.func.isRequired,
  addFavorites: PropTypes.func.isRequired,
}

export default Main