import React from "react";
import s from './HeaderTop.module.scss'
import HeaderNav from "./HeaderNav/HeaderNav";
import facebook from './imgs/facebook-top-icon.png'
import dribble from './imgs/top-dribble-icon.png'
import twitter from './imgs/top-twitter-icon.png'
import mail from './imgs/top-mail-icon.png'
import vimeo from './imgs/top-vimeo-icon.png'
import logo from './imgs/Logo.png'
import bg from '../../imgsBase/bg-slider-second.jpg'



class HeaderTop extends React.Component {

  render() {
    return (
     <>
       <section className={s.headerTop}>
         <div className={s.headerFirstWrapper}>
         <span className={s.headerRedLine}></span>
         <div className={s.container}>
           <div className={s.headerWrapper}>
             <div className={s.headerSocial}>
               <a href='#'><img src={facebook} alt="icon facebook" title="Facebook"/></a>
               <a href='#'><img src={dribble} alt="icon dribble" title="Dribble"/></a>
               <a href='#'><img src={twitter} alt="icon twitter" title="Twitter"/></a>
               <a href='#'><img src={mail} alt="icon mail" title="Email"/></a>
               <a href='#'><img src={vimeo} alt="icon vimeo" title="Vimeo"/></a>
             </div>
             <div className={s.headerLog}>
               <span><a href="">Login</a></span>
               <span>/</span>
               <span><a href="">Register</a></span>
             </div>
           </div>
         </div>
         </div>
          <div className={s.headerSecondWrapper} style={{backgroundImage: `url(${bg})`}}>
            <div className={s.container}>
              <div className={s.headerWrapper}>
              <a href="#" className={s.headerLogo}>
                <img className={s.headerImg} src={logo} alt="Icon logo" title="M Store" />
                <h1 className={s.headerLogoName}>Store</h1>
              </a>
              <HeaderNav />
              </div>
            </div>
          </div>
       </section>
     </>
    )
  }
}

export default HeaderTop