import React from "react";
import s from "../HeaderTop.module.scss";

class HeaderNav extends React.Component {
  render() {
    return(
     <nav className={s.headerMain}>
       <ul className={s.headerMainItems}>
         <li><a href="#">Home</a></li>
         <li><a href="#">CD's</a></li>
         <li><a href="#">DVD's</a></li>
         <li><a href="#">News</a></li>
         <li><a href="#">Portfolio</a></li>
         <li><a href="#">Contact us</a></li>
       </ul>
     </nav>
    )
  }
}

export default HeaderNav