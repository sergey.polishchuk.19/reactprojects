import React, {Component} from "react";
import s from './HeaderMiddle.module.scss'
import firstSlide from  './imgs/bg-slider-one.jpg'
import secondSlide from  './imgs/bg-slider-second.jpg'
import thirdSlide from  './imgs/bg-slider-second.jpg'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from 'react-slick'

const images = [firstSlide, secondSlide, thirdSlide]

class HeaderMiddle extends Component {
  render() {
    const settings = {
      dots: false,
      infinite: true,
      speed: 300,
      centerMode: true,
      slidesToShow: 1,
      slidesToScroll: 1,
    };
    return (
     <div className={s.sliderWrapper}>
       <Slider {...settings}>
         {images.map(img =>(
          <div className={s.sliderItem}>
            <img width="100%" height="100%" src={img} alt={img}/>
          </div>
         ))}
       </Slider>
     </div>
    );
  }
}

export default HeaderMiddle