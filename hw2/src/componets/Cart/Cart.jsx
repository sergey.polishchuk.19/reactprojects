import React from "react";
import PropTypes from 'prop-types'
import s from './Cart.module.scss'
import iconCart from './icon/cart.png'

class Cart extends React.Component{

  render() {
    const {carts} = this.props
    return(
     <section className={s.cartWrapper}>
       <div className={s.container}>
       <div className={s.cartItem}>
         <div>
           <img src={iconCart} alt="icon"/>
         </div>
         <p>Cart (<span>{carts.length}</span>)</p>
       </div>
       </div>
     </section>
    );
  }
}

Cart.proptype = {
  carts: PropTypes.array.isRequired
}

export default Cart