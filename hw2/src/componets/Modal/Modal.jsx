import React from "react";
import PropTypes from "prop-types";
import s from "./Modal.module.scss"
import closeIcon from './icon/close.png'

class Modal extends React.Component{
  render() {
    const {name, modal, actions, closeModal} = this.props
    if(!modal) return null
    return(
     <section>
       <div className={s.root}>
         <div className={s.background} />
         <div className={s.content}>
           <div className={s.closeWrapper}>
             <button onClick={closeModal}>
               <img width='18' src={closeIcon} alt="icon close"/>
             </button>
           </div>
           <h2>Add to your cart <span className={s.nameAlbum}>{name}</span>?</h2>
           <div className={s.buttonContainer}>
             {actions}
           </div>
         </div>
       </div>

     </section>
    )
  }
}

Modal.proptype = {
  name: PropTypes.string.isRequired,
  modal: PropTypes.bool.isRequired,
  actions: PropTypes.element.isRequired,
  closeModal: PropTypes.func.isRequired,
}

export default Modal