import React from "react";
import {ReactComponent as StarSolid} from "./icon/star-solid.svg";
import {ReactComponent as StarAdd} from "./icon/star-solid-add.svg";
import PropTypes from "prop-types";

class Favorites extends React.Component{
  render() {
    const {name, isFavorite, addFavorites} = this.props
    return(
     <div>
       {!isFavorite ? <StarSolid style={{ width: '25px', cursor: 'pointer' }} onClick={()=>{addFavorites(name)}} />
        :
        <StarAdd style={{ width: '25px', cursor: 'pointer' }} onClick={()=>{addFavorites(name)}} />}
     </div>
    )
  }
}

Favorites.proptype = {
  name: PropTypes.string.isRequired,
  isFavorite: PropTypes.bool.isRequired,
  addFavorites: PropTypes.func.isRequired,
}

export default Favorites