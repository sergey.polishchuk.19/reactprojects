import React from "react";
import ItemsContainer from "../../components/ItemsContainer/ItemsContainer";

function MainPage (props){
  const {cards,
    addCarts,
    openModal,
    } = props
  return(
   <div>
   <ItemsContainer  cards={cards} addCarts={addCarts} openModal={openModal} />
   </div>
  )
}

export default MainPage