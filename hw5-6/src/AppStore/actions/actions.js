export const GET_ITEMS = 'GET_ITEMS';

export const SET_IS_OPEN = 'SET_IS_OPEN'

export const TOGGLE_FAV = 'TOGGLE_FAV';
export const ADD_TO_CART = 'ADD_TO_CART';
export const DELETE_FROM_CART = 'DELETE_FROM_CART';
export const CLEAR_CART = 'CLEAR_CART'
export const GET_DATA_TO_CART = 'GET_DATA_TO_CART';
export const GET_DATA_TO_FAV = 'GET_DATA_TO_FAV';