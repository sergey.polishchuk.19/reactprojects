import { ADD_TO_CART, DELETE_FROM_CART, GET_DATA_TO_CART, CLEAR_CART } from "../actions/actions"

const initialState = {
  cartItems:[],
}
console.log(initialState)

export const cartsReducer = (state = initialState, action)=> {
  if (action.type === ADD_TO_CART) {
    const index = state.cartItems.findIndex(({name}) => {
      return action.payload === name
    })
    if (index === -1) {
      return {...state, cartItems: [...state.cartItems, {name: action.payload, count: 1}]}
    } else {
      const newState = [...state.cartItems]
      newState[index].count = newState[index].count + 1
      return {...state, cartItems: [...newState]}
    }
  } else if (action.type === DELETE_FROM_CART) {
    const index = state.cartItems.findIndex(({name}) => {
      return action.payload === name
    })
    const newState = [...state.cartItems];
    console.log(newState)
    if (newState[index].count === 1) {
      console.log(newState[index])
      newState.splice(index, 1)
    } else {
      newState[index].count = newState[index].count - 1;
    }

    return {...state, cartItems: [...newState]}

  } else if (action.type === GET_DATA_TO_CART) {
    return {...state, cartItems: [...action.payload]}
  }else if(action.type === CLEAR_CART){
    return {
      ...state,
      cartItems: action.data
    }


  }
  else{
    return state
  }
}