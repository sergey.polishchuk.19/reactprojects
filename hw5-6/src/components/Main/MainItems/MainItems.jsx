import React from "react";
import s from './MainItems.module.scss'
import Item from "./Item";

function MainItems (props) {
  const {cards, addCarts, openModal} = props

    return (
     <>
       <h2 className={s.playlistLogo}>Latest Arrivals in musica</h2>
       <section className={s.playlist}>
         {cards && cards.map((elements) => <Item openModal={openModal} {...elements} addCarts={addCarts} />)}
       </section>
     </>
    );
  }

export default MainItems