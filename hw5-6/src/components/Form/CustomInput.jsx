import React from "react";
import s from './Form.module.scss'

function CustomInput (props){
  const { type, placeholder, field } = props
  return (
   <>
     <input
      className={s.input}
      type={type}
      {...field}
     />
   </>
  )
}

export default CustomInput