import React from "react";
import {Form, Formik, Field, ErrorMessage} from "formik";
import * as yup from 'yup'
import s from './Form.module.scss'
import {useSelector, useDispatch} from "react-redux";
import {clearCart} from "../../AppStore/actionsCreator/actionsCreator";
import CustomInput from './CustomInput'

const ERROR_MESSAGE = 'Wrong data'

function Forms(props) {
  const {carts} = props
  const initialValues = {
    name: '',
    secondName: '',
    age: '',
    address: '',
    phone: '',
  }
  const dispatch = useDispatch()

  const handleSubmit = values => {
    const current = carts.map(({name}) => name)
    dispatch(clearCart([]))
    console.log(values, current)
    localStorage.removeItem('carts')
  }


  const validationSchema = yup.object().shape({
    name: yup.string().required(ERROR_MESSAGE),
    secondName: yup.string().required(ERROR_MESSAGE),
    age: yup.string().required(ERROR_MESSAGE),
    address: yup.string().required(ERROR_MESSAGE),
    phone: yup.number().required(ERROR_MESSAGE).min(10, 'Enter your phone'),
  })
  return (
   <Formik
    initialValues={initialValues}
    validationSchema={validationSchema}
    onSubmit={handleSubmit}
   >
     {(props) => {
       const {values} = props
       return (
        <div className={s.form}>
          <p>
            <label className={s.label} htmlFor='name'>Name</label>
            <Field
             type="text"
             name="name"
             value={values.name}
             component={CustomInput}
             error={props.errors.name}
            />
            <ErrorMessage name={'name'} render={(ERROR_MESSAGE) => <span className={s.error}>{ERROR_MESSAGE}</span>}/>
          </p>
          <p>
            <label className={s.label} htmlFor='name'>Second name</label>
            <Field
             type="text"
             name="secondName"
             component={CustomInput}
             value={values.secondName}
             error={props.errors.name}
            />
            <ErrorMessage name={'secondName'} render={(ERROR_MESSAGE) => <span className={s.error}>{ERROR_MESSAGE}</span>}/>
          </p>
          <p>
            <label className={s.label} htmlFor='name'>Age</label>
            <Field
             type="text"
             name="age"
             value={values.age}
             component={CustomInput}
             error={props.errors.name}
            />
            <ErrorMessage name={'age'} render={(ERROR_MESSAGE) => <span className={s.error}>{ERROR_MESSAGE}</span>}/>
          </p>
          <p>
            <label className={s.label} htmlFor='name'>Address</label>
            <Field
             type="text"
             name="address"
             value={values.address}
             component={CustomInput}
             error={props.errors.name}
            />
            <ErrorMessage name={'address'} render={(ERROR_MESSAGE) => <span className={s.error}>{ERROR_MESSAGE}</span>}/>
          </p>
          <p>
            <label className={s.label} htmlFor='name'>Phone</label>
            <Field
             type="text"
             name="phone"
             value={values.phone}
             component={CustomInput}
             error={props.errors.name}
            />
            <ErrorMessage name={'phone'} render={(ERROR_MESSAGE) => <span className={s.error}>{ERROR_MESSAGE}</span>}/>
          </p>
          <button
           className={s.submit}
           onClick={()=> handleSubmit(values)}
           type="submit"
          >
            Send
          </button>
        </div>
       )

     }}
   </Formik>
  )
}

export default Forms