import React from "react";
import Main from "../Main/Main";

function ItemsContainer (props) {
  const {cards, addCarts, openModal} = props
  return(
   <>
   <Main cards={cards} addCarts={addCarts} openModal={openModal} />
   </>
  )
}

export default ItemsContainer